@extends('master.master')

@section('title', 'List Cast')

@section('content')

<div class="m-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Data Cast</h3>

        </div>
        <!-- /.card-header -->
        <div class="card-body">

            @if(session('status'))
                <div class="alert alert-success m-3">
                    {{ session('status') }}
                </div>
            @endif

            <a href="{{ route('cast.create') }}" class="btn btn-primary mb-4" style="width: 150px">Create Data</a>
            <table class="table table-bordered">

                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @forelse($cast as $key => $casts)
                        <tr>
                            <td>{{ $casts->id }}</td>
                            <td>{{ $casts->nama }}</td>
                            <td>{{ $casts->umur }}</td>
                            <td>{{ $casts->bio }}</td>
                            <td>
                                <form action="{{ route('cast.destroy', $casts->id )}}" method="POST">
                                    <a href="{{ route('cast.show', $casts->id )}}" class="btn btn-info">Show</a>
                                    <a href="{{ route('cast.edit', $casts->id )}}" class="btn btn-warning">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">
                                <center>No data</center>
                            </td>
                        </tr>
                    @endforelse



                </tbody>
            </table>
        </div>
        <!-- /.card-body -->

    </div>
</div>
@endsection
