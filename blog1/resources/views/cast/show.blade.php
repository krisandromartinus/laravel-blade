@extends('master.master')

@section('title', 'Show Cast')

@section('content')

<div class="m-3">

<div class="card">
    <div class="card-header">
      <h3 class="card-title">Detail ID : {{ $cast->id }}</h3>
    </div>
    <div class="card-body p-3 m-1">
        <a href="{{ route('cast.index') }}" class="btn btn-primary mb-1" style="width: 150px">Back</a>
        <h4>Nama    : {{$cast->nama}}</h4>
        <p>Umur     : {{$cast->umur}}</p>
        <p>Bio      : {{$cast->bio}}</p>
    </div>
    
    <!-- /.card-body -->
  </div>

</div>
@endsection
