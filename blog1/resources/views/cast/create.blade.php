@extends('master.master')

@section('title', 'Create Cast')

@section('content')
<div class="m-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create Cast</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            
            <a href="{{ route('cast.index') }}" class="btn btn-warning mb-1" style="width: 150px">Back</a>
            <form action="/cast" method="post">
                @csrf
                <div class="form-group">
                    <label for="castname">Nama</label>
                    <input type="text" id="nama" name="nama" value="{{ old('nama') }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" id="umur" name="umur" value="{{ old('umur') }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea id="bio" class="form-control" name="bio" rows="4">{{ old('bio') }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            @if($errors->any())
                <div class="alert alert-danger m-3">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
        <!-- /.card-body -->
    </div>
</div>
@endsection
