<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = DB::table('cast')->get();

        return view('cast.cast', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert(
            [
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,

            ]
        );

        return redirect('cast')->with('status', 'Cast Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        if ($cast === null) {
            return redirect('cast');
        }
        return view('cast.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $cast = DB::table('cast')->where('id', $id)->first();

        if ($cast === null) {
            return redirect('cast');
        }
        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        $cast = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio,

            ]);
        return redirect('cast')->with('status', 'Cast ID ' . $id . ' Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $cast = DB::table('cast')->where('id', $id)->delete();
        if ($cast === null) {
            return redirect('cast');
        }
        return redirect('cast')->with('status', 'Cast ID ' . $id . ' Berhasil Dihapus');
    }
}
